<?php
$continentAnimals = array(
    'Eurasia' => array('Cervus elaphus', 'Ailuropoda melanoleuca', 'Camelus',
        'Panthera uncia', 'Ovis orientalis orientalis', 'Martes zibellina', 'Canis lupus'),
    'Africa' => array('Mammuthus columbi', ' Loxodonta', 'Gorilla', 'Panthera pardus', 'Crocuta crocuta'),
    'North America' => array('Taxidea taxus', 'Castor', 'Bassariscus', 'Rangifer tarandus'),
    'South America' => array('Lagostomus maximus', 'Chrysocyon brachyurus', 'Choloepus didactylus',
        'Hydrochoerus hydrochaeris', 'Pecari tajacu'),
    'Australia' => array('Peramelemorphia', 'Vombatidae', 'Dendrolagus', 'Macropus fuliginosus', 'Petaurus breviceps'),
    'Arctic' => array('Ursus maritimus', 'Lepus timidus', 'Vulpes lagopus', 'Odobenus rosmarus'),
    'Antarctica' => array('Spheniscus mendiculus', 'Physeter macrocephalus', 'Ommatophoca rossii')
);

$twoWorldAnimals = array(); // An array of animals that have two words in the name
$newAnimals = array(); // array for new animal's name
$secondWords = array(); // the second word of the animal's name

foreach ($continentAnimals as $continent => $animals) {
    foreach ($animals as $animal) {
        $byWord = explode(' ', trim($animal));
        if (count($byWord) == 2) {
            $twoWorldAnimals[$continent][] = $animal;
            $newAnimals[$continent][] = $byWord[0];
            $secondWords[] = $byWord[1];
        }
    }
}

shuffle($secondWords); // random sort the second words

foreach ($newAnimals as &$firstWord) {
    foreach ($firstWord as &$animal) {
        $animal .= ' ' . array_shift($secondWords);
    }
}

// display Fantasy animals
echo "<h1>Fantasy animals</h1>";
displayAnimals($newAnimals);

// display Real animals
echo "<h1>Real animals</h1>";
displayAnimals($twoWorldAnimals);

function displayAnimals($animals)
{
    foreach ($animals as $continent => $animal) {
        echo "<h2>" . $continent . "</h2>";
        echo implode(', ', $animal);
    }
}